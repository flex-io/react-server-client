module.exports = {
  error_file: "err.log",
  out_file: "out.log",
  log_file: "combined.log",
  time: true,
  apps: [
    {
      name: "myapp-api",
      script: "./server/server.js",
      watch: true,
      env: {
        DB_USER: "postgres",
        DB_PASS: "postgres",
        DB_HOST: "localhost",
        DB_PORT: 5432,
        DB_NAME: "postgres",
        SUPERHEROAPI_TOKEN: 4190104761000825,
        NODE_ENV: "development",
      },
      env_production: {
        DB_USER: "postgres",
        DB_PASS: "QWEqwe123",
        DB_HOST: "coco-instance.cfijsfk7f2ng.ap-southeast-2.rds.amazonaws.com",
        DB_PORT: 5432,
        DB_NAME: "coco",
        SUPERHEROAPI_TOKEN: 4190104761000825,
        NODE_ENV: "development",
        NODE_ENV: "production",
      },
    },
    {
      name: "myapp-client",
      script: "yarn",
      args: "start",
      watch: true,
      env: {
        NODE_ENV: "development",
      },
      env_production: {
        NODE_ENV: "production",
      },
    },
  ],
};
